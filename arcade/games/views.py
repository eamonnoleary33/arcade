from django.shortcuts import render

def index(request):
    return render(request, 'index.html')

import random
from rest_framework.decorators import api_view
from rest_framework.response import Response

@api_view(['POST'])
def start_game(request):
    # Deal initial cards to the user and the computer
    user_cards = [deal_card(), deal_card()]
    computer_cards = [deal_card(), deal_card()]

    # Calculate initial scores
    user_score = calculate_score(user_cards)
    computer_score = calculate_score(computer_cards)

    game_data = {
        'user_cards': user_cards,
        'computer_cards': [computer_cards[0]],  # Only reveal first card of computer
        'user_score': user_score,
        'computer_score': computer_score,
        'is_game_over': False
    }

    return Response(game_data)

@api_view(['POST'])
def hit(request):
    user_cards = request.data['user_cards']
    user_cards.append(deal_card())
    user_score = calculate_score(user_cards)

    game_data = {
        'user_cards': user_cards,
        'user_score': user_score,
        'computer_cards': request.data['computer_cards'],
        'computer_score': request.data['computer_score'],
        'is_game_over': False
    }

    if user_score >= 21:
        game_data['is_game_over'] = True

    return Response(game_data)

@api_view(['POST'])
def stand(request):
    user_cards = request.data['user_cards']
    computer_cards = request.data['computer_cards']
    computer_score = request.data['computer_score']

    # Let the computer draw cards until it reaches a score of 17 or higher
    while computer_score < 17:
        computer_cards.append(deal_card())
        computer_score = calculate_score(computer_cards)

    game_data = {
        'user_cards': user_cards,
        'user_score': request.data['user_score'],
        'computer_cards': computer_cards,
        'computer_score': computer_score,
        'is_game_over': True
    }

    return Response(game_data)

@api_view(['POST'])
def restart_game(request):
    return start_game(request)

# Helper functions

def deal_card():
    # Select a random card from the standard deck
    cards = [11, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10]
    return random.choice(cards)

def calculate_score(cards):
    # Calculate the total score of a hand, handle the presence of an Ace
    score = sum(cards)
    if score == 21 and len(cards) == 2:
        return 0  # Blackjack
    elif 11 in cards and score > 21:
        cards.remove(11)
        cards.append(1)
        score = sum(cards)  # Convert Ace from 11 to 1
    return score
