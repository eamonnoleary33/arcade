from django.urls import path
from .views import *
from django.contrib.auth import views as auth_views
from django.contrib.auth.views import LoginView

urlpatterns = [
    path('', index, name='index'),
    path('api/blackjack/start/', start_game, name='start_game'),
    path('api/blackjack/hit/', hit, name='hit'),
    path('api/blackjack/stand/', stand, name='stand'),
    path('api/blackjack/restart/', restart_game, name='restart_game'),
]
