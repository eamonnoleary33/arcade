from django.db import models

class Game(models.Model):
    user_cards = models.JSONField()
    computer_cards = models.JSONField()
    user_score = models.IntegerField()
    computer_score = models.IntegerField()
    is_game_over = models.BooleanField(default=False)
