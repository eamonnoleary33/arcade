import React, { useState, useEffect } from 'react';
import axios from 'axios'; // Import Axios for making HTTP requests

const GameComponent = () => {
    const [gameState, setGameState] = useState(null);

    useEffect(() => {
        // Fetch initial game state when component mounts
        axios.post('/api/blackjack/start/')
            .then(response => setGameState(response.data))
            .catch(error => console.error('Error:', error));
    }, []);

    const handleHit = () => {
        axios.post('/api/blackjack/hit/')
            .then(response => setGameState(response.data))
            .catch(error => console.error('Error:', error));
    };

    const handleStand = () => {
        axios.post('/api/blackjack/stand/')
            .then(response => setGameState(response.data))
            .catch(error => console.error('Error:', error));
    };

    const handleRestart = () => {
        axios.post('/api/blackjack/restart/')
            .then(response => setGameState(response.data))
            .catch(error => console.error('Error:', error));
    };

    return (
        <div>
            {/* Render game interface based on gameState */}
            {/* For example, render user cards, computer cards, scores */}
            <button onClick={handleHit}>Hit</button>
            <button onClick={handleStand}>Stand</button>
            <button onClick={handleRestart}>Restart</button>
        </div>
    );
};

export default GameComponent;
