// App.js

import React from 'react';
import GameComponent from './GameComponent'; // Import the GameComponent

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>Blackjack Game</h1>
      </header>
      <main>
        <GameComponent /> {/* Render the GameComponent */}
      </main>
    </div>
  );
}

export default App;
